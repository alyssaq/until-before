import test from 'ava'
import untilBefore from '..'

test('keep before text', (t) => {
  t.is('foobar'::untilBefore('bar'), 'foo')
})

test('keep before emoji', (t) => {
  t.is('foo💩bar'::untilBefore('💩'), 'foo')
})
