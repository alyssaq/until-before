# until-before

## API

### `operand::untilBefore(pattern)`

Returns the trimmed string.

Looks for a pattern in a string and, if the pattern is found, crops the string before the first occurrence of the pattern, removing the trailing remainder.

Designed for use with `::` function bind syntax, as the `this` property should be the string to trim.

#### operand

String to trim.

#### pattern

String to look for and trim before.

## Use Case

```js
import untilBefore from 'until-before'

const filepath = '~/app/node_modules/foo/bar.js'

filepath::untilBefore('node_modules/')
// -> '~/app/`
```

## Peer Dependencies

This package uses Babel to transpile its code to be compatible with the node version it is installed on. Ensure you have installed the necessary peer dependencies.

```
npm install babel-cli babel-preset-env babel-plugin-transform-function-bind
```

On some older Node environments, e.g. 4.x and earlier, you may also require system-wide polyfills.

```
npm instal babel-polyfill
```

And then load the `babel-polyfill` library before loading this package.

## See Also

- [from-before](https://www.npmjs.com/package/from-before) ⟼
- [from-after](https://www.npmjs.com/package/from-after) ⇤
- [until-before](https://www.npmjs.com/package/until-before) ⇥
- [until-after]((https://www.npmjs.com/package/until-after)) ⟻

```js
const text = 'goodbye cruel world'

text::fromBefore('cruel')  // 'cruel world'
text::fromAfter('cruel')   // ' world'
text::untilBefore('cruel') // 'goodbye '
text::untilAfter('cruel')  // 'goodbye cruel'
```

## Colophon

Made by Sebastiaan Deckers in Singapore 🇸🇬
